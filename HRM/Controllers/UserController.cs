﻿using System.Collections.Generic;
using HRM.Models.Entity;
using HRM.Service.UserService;
using Microsoft.AspNetCore.Mvc;

namespace HRM.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public ViewResult Index()
        {
            return
                View(_userService.GetAll());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(User user)
        {
            _userService.Add(user);
            _userService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var getId = _userService.GetById(id);
            return View(getId);
        }

        [HttpPost]
        public IActionResult Edit(User user)
        {
            _userService.Update(user);
            _userService.Save();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            _userService.Delete(id);
            _userService.Save();
            return RedirectToAction("Index");
        }
    }
}