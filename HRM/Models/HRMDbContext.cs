﻿using HRM.Models.Entity;
using Microsoft.EntityFrameworkCore;

namespace HRM.Models
{
    public class HRMDbContext : DbContext
    {
        public HRMDbContext(DbContextOptions<HRMDbContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id).HasColumnName("Id");
                entity.Property(e => e.UserName).HasColumnName("UserName");
                entity.Property(e => e.FullName).HasColumnName("FullName");
                entity.Property(e => e.PassWord).HasColumnName("PassWord");
                entity.Property(e => e.IsActive).HasColumnName("IsActive");
                entity.Property(e => e.Email).HasColumnName("Email");
            });
        }
    }
}