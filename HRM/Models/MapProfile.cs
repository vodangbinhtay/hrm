﻿using AutoMapper;
using HRM.Models.Entity;
using HRM.Models.ViewModel;

namespace HRM.Models
{
    public class MapProfile : Profile
    {
        public MapProfile()
        {
            CreateMap<User, UserViewModel>()
                .ForMember(d => d.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(d => d.FullName, opt => opt.MapFrom(src => src.FullName))
                .ForMember(d => d.UserName, opt => opt.MapFrom(src => src.UserName))
                .ForMember(d => d.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(d => d.PassWord, opt => opt.MapFrom(src => src.PassWord))
                .ForMember(d => d.IsActive, opt => opt.MapFrom(src => src.IsActive));
            CreateMap<UserViewModel, User>()
                .ForMember(d => d.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(d => d.FullName, opt => opt.MapFrom(src => src.FullName))
                .ForMember(d => d.UserName, opt => opt.MapFrom(src => src.UserName))
                .ForMember(d => d.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(d => d.PassWord, opt => opt.MapFrom(src => src.PassWord))
                .ForMember(d => d.IsActive, opt => opt.MapFrom(src => src.IsActive));
        }
    }
}