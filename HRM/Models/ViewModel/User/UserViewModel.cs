﻿using System.ComponentModel.DataAnnotations;

namespace HRM.Models.ViewModel
{
    public class UserViewModel
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        [Required] public string UserName { get; set; }

        [Required] [EmailAddress] public string Email { get; set; }

        [Required]
        [StringLength(30, ErrorMessage = "Password must be least 6 character long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string PassWord { get; set; }

        public bool IsActive { get; set; }
    }
}