﻿using HRM.Models.Entity;
using HRM.Repository.GenericRepository;

namespace HRM.Repository.UserRepository
{
    public interface IUserRepository : IGenericRepository<User>
    {
    }
}