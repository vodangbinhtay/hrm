﻿using HRM.Models;
using HRM.Models.Entity;
using HRM.Repository.GenericRepository;

namespace HRM.Repository.UserRepository
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        public UserRepository(HRMDbContext context) : base(context)
        {
        }
    }
}