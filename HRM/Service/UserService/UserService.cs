﻿using System;
using System.Collections.Generic;
using System.Linq;
using HRM.Models;
using HRM.Models.Entity;
using HRM.UnitOfWork;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Core;

namespace HRM.Service.UserService
{
    public class UserService : IUserService
    {
        private IUnitOfWork _unitOfWork;
        private HRMDbContext _context;
        private readonly ILogger<UserService> _logger;

        public UserService(IUnitOfWork unitOfWork, HRMDbContext context, ILogger<UserService> logger)
        {
            _unitOfWork = unitOfWork;
            _context = context;
            _logger = logger;
        }

        public User GetById(int id)
        {
            try
            {
                return _unitOfWork.UserReponsitory.GetById(id);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, "Get Fail");
                throw;
            }
        }

        public IEnumerable<User> GetAll()
        {
            try
            {
                return _unitOfWork.UserReponsitory.GetAll();
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, "Get Fail");
                throw;
            }
        }

        public void Add(User user)
        {
            try
            {
                _unitOfWork.UserReponsitory.Add(user);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, "add Fail");
                throw;
            }
        }

        public void Delete(int id)
        {
            try
            {
                _unitOfWork.UserReponsitory.Delete(id);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, "delete fail");
                throw;
            }
        }

        public void Update(User user)
        {
            try
            {
                _unitOfWork.UserReponsitory.Update(user);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message, "update fail");
                throw;
            }
        }

        public void Save()
        {
            _unitOfWork.UserReponsitory.Save();
        }
    }
}