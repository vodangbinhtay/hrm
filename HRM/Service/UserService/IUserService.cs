﻿using System.Collections.Generic;
using HRM.Models.Entity;

namespace HRM.Service.UserService
{
    public interface IUserService
    {
        User GetById(int id);
        IEnumerable<User> GetAll();
        void Add(User user);
        void Delete(int id);
        void Update(User user);
        void Save();
    }
}