﻿using System;
using HRM.Models;
using HRM.Models.Entity;
using HRM.Repository.GenericRepository;

namespace HRM.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private HRMDbContext _context;

        public UnitOfWork(HRMDbContext context)
        {
            _context = context;
            InitReponsitory();
        }

        private bool _disposed = false;

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IGenericRepository<User> UserReponsitory { get; private set; }

        private void InitReponsitory()
        {
            UserReponsitory = new GenericRepository<User>(_context);
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}