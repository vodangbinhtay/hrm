﻿using System;
using HRM.Models.Entity;
using HRM.Repository.GenericRepository;

namespace HRM.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<User> UserReponsitory { get; }
        void Save();
    }

}